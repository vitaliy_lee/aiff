use std::convert::TryInto;
use std::fmt::{Debug, Formatter};
use std::fs;
use std::io;
use std::io::prelude::*;
use std::io::BufWriter;
use std::io::Write;
use std::mem;
use std::path;
use std::string;

mod float80;
mod pstring;
mod util;

const CHUNK_HEADER_SIZE: usize = 8;

#[repr(C)]
#[derive(Debug)]
struct ChunkHeader {
    id: [u8; 4], // u32
    size: i32,
}

#[repr(C)]
#[derive(Debug)]
struct FormatHeader {
    id: [u8; 4], // u32
    size: i32,
    format: [u8; 4],
}

#[repr(C)]
#[derive(Debug)]
struct CommonChunkData {
    num_channels: i16,
    num_sample_frames: [u8; 4],
    sample_size: i16,
    sample_rate: [u8; 10],
}

#[repr(C)]
#[derive(Debug)]
struct MarkerData {
    id: i16,
    position: u32,
    name: Vec<u8>,
}

#[repr(C)]
#[derive(Debug)]
pub struct CommentData {
    time_stamp: u32,
    marker: MarkerID,
    count: u16,
    text: Vec<u8>,
}

#[repr(C)]
#[derive(Debug)]
struct LoopData {
    play_mode: i16,
    begin_loop: MarkerID,
    end_loop: MarkerID,
}

#[repr(C)]
#[derive(Debug)]
struct InstrumentChunkData {
    base_frequency: u8, //base_note
    detune: u8,
    low_frequency: u8,  //low_note
    high_frequency: u8, //high_note
    low_velocity: u8,
    high_velocity: u8,
    gain: i16,
    sustain_loop: LoopData,
    release_loop: LoopData,
}

type MarkerID = u16;

#[derive(Debug)]
pub struct Marker {
    id: MarkerID,
    position: u32,
    name: String,
}

impl Marker {
    pub fn new(id: MarkerID, position: u32, name: String) -> Marker {
        if id == 0 {
            panic!("Marker ID must be not zero");
        }

        Marker { id, position, name }
    }
}

#[derive(Debug)]
pub enum PlayMode {
    NoLooping = 0,
    ForwardLooping = 1,
    ForwardBackwardLooping = 2,
}

impl PlayMode {
    fn from(input: i16) -> io::Result<PlayMode> {
        match input {
            0 => Ok(PlayMode::NoLooping),
            1 => Ok(PlayMode::ForwardLooping),
            2 => Ok(PlayMode::ForwardBackwardLooping),
            _ => Err(io::Error::new(
                io::ErrorKind::Other,
                "unsupported play mode type",
            )),
        }
    }
}

#[derive(Debug)]
pub struct Loop {
    play_mode: PlayMode,
    begin_loop: MarkerID,
    end_loop: MarkerID,
}

impl Loop {
    fn from(data: LoopData) -> io::Result<Loop> {
        let play_mode = PlayMode::from(data.play_mode)?;

        let audio_loop = Loop {
            play_mode: play_mode,
            begin_loop: data.begin_loop,
            end_loop: data.end_loop,
        };

        return Ok(audio_loop);
    }
}

#[derive(Debug)]
pub struct Comment {
    time_stamp: u32,
    marker: MarkerID,
    text: String,
}

#[derive(Debug)]
pub struct Instrument {
    base_frequency: u8,
    detune: u8,
    low_frequency: u8,
    high_frequency: u8,
    low_velocity: u8,
    high_velocity: u8,
    gain: i16,
    sustain_loop: Loop,
    release_loop: Loop,
}

pub enum Chunk {
    CommonChunk {
        num_channels: u16,
        num_sample_frames: u32,
        sample_size: u16,
        sample_rate: f64,
    },
    SoundDataChunk {
        offset: u32,
        block_size: u32,
        data: Vec<u8>,
    },
    MarkerChunk {
        markers: Vec<Marker>,
    },
    ApplicationSpecificChunk {
        application_signature: String,
        data: Vec<u8>,
    },
    InstrumentChunk {
        base_frequency: u8,
        detune: u8,
        low_frequency: u8,
        high_frequency: u8,
        low_velocity: u8,
        high_velocity: u8,
        gain: i16,
        sustain_loop: Loop,
        release_loop: Loop,
    },
    CommentsChunk {
        comments: Vec<Comment>,
    },
    ID3Chunk {
        data: Vec<u8>,
    },
    // MIDIDataChunk
    // AudioRecordingChunk
    NameChunk {
        text: String,
    },
    AuthorChunk {
        text: String,
    },
    CopyrightChunk {
        text: String,
    },
    AnnotationChunk {
        text: String,
    },
    UnsupportedChunk {
        id: String,
        size: usize,
        data: Vec<u8>,
    },
}

impl Chunk {
    fn from(data: &[u8]) -> io::Result<(Chunk, usize)> {
        log::debug!("{:x?}", &data[..CHUNK_HEADER_SIZE]);

        let mut chunk_header = util::read_struct::<ChunkHeader, &[u8]>(data)?;
        chunk_header.size = chunk_header.size.to_be();

        log::debug!("{:?}", chunk_header);

        let start = CHUNK_HEADER_SIZE;
        let end = CHUNK_HEADER_SIZE + chunk_header.size as usize;

        log::trace!("[{}..{}]", start, end);

        let slice = &data[start..end];

        let chunk = 
        match &chunk_header.id {
            b"COMM" => Chunk::parse_common_chunk(slice)?,
            b"SSND" => Chunk::parse_sound_data_chunk(slice)?,
            b"MARK" => Chunk::parse_marker_chunk(slice)?,
            b"APPL" => Chunk::parse_application_specific_chunk(slice)?,
            b"INST" => Chunk::parse_instrument_chunk(slice)?,
            b"COMT" => Chunk::parse_comments_chunk(slice)?,
            b"ID3 " => Chunk::parse_id3_chunk(slice)?,
            b"NAME" => Chunk::parse_name_chunk(slice)?,
            b"AUTH" => Chunk::parse_author_chunk(slice)?,
            b"(c) " => Chunk::parse_copyright_chunk(slice)?,
            b"ANNO" => Chunk::parse_annotation_chunk(slice)?,
            _ => Chunk::parse_unsupported_chunk(chunk_header, slice)?,
        };

        Ok((chunk, slice.len()))
    }

    fn parse_unsupported_chunk(header: ChunkHeader, data: &[u8]) -> io::Result<Chunk> {
        let id = String::from_utf8(header.id.to_vec()).unwrap();

        let chunk = Chunk::UnsupportedChunk {
            id: id,
            size: header.size as usize,
            data: data.to_vec(),
        };

        return Ok(chunk);
    }

    fn parse_common_chunk(data: &[u8]) -> io::Result<Chunk> {
        let chunk_data = util::read_struct::<CommonChunkData, &[u8]>(data)?;

        let chunk = Chunk::CommonChunk {
            num_channels: chunk_data.num_channels.to_be() as u16,
            num_sample_frames: u32::from_be_bytes(chunk_data.num_sample_frames),
            sample_size: chunk_data.sample_size.to_be() as u16,
            sample_rate: util::f64_from_extended(chunk_data.sample_rate),
        };

        return Ok(chunk);
    }

    fn parse_sound_data_chunk(data: &[u8]) -> io::Result<Chunk> {
        let data = Vec::from(&data[8..]);

        let chunk = Chunk::SoundDataChunk {
            offset: 0,
            block_size: 0,
            data,
        };

        return Ok(chunk);
    }

    fn parse_marker_chunk(data: &[u8]) -> io::Result<Chunk> {
        let buffer: [u8; 2] = data[..2].try_into().unwrap();

        let num_markers = u16::from_be_bytes(buffer) as usize;

        let mut markers = Vec::with_capacity(num_markers);

        let mut start = 2;

        for _i in 0..num_markers {
            let buffer: [u8; 2] = data[start..start + 2].try_into().unwrap();
            let id = i16::from_be_bytes(buffer) as u16;

            start = start + 2;

            let buffer: [u8; 4] = data[start..start + 4].try_into().unwrap();
            let position = u32::from_le_bytes(buffer);

            start = start + 4;

            let name = pstring::parse_pstring(&data[start..]).unwrap();

            let mut length = name.len() + 1;

            // Count pad byte
            if length % 2 != 0 {
                length += 1;
            }

            start = start + length;

            let marker = Marker { id, position, name };

            markers.push(marker);
        }

        let chunk = Chunk::MarkerChunk { markers };

        return Ok(chunk);
    }

    fn parse_comments_chunk(data: &[u8]) -> io::Result<Chunk> {
        let buffer: [u8; 2] = data[..2].try_into().unwrap();
        let num_comments = u16::from_be_bytes(buffer) as usize;

        let mut comments = Vec::with_capacity(num_comments);

        let mut start = 2;

        for _i in 0..num_comments {
            let buffer: [u8; 4] = data[start..start + 4].try_into().unwrap();
            let time_stamp = u32::from_be_bytes(buffer);

            start += 4;

            let buffer: [u8; 2] = data[start..start + 2].try_into().unwrap();
            let marker = u16::from_le_bytes(buffer);

            start += 2;

            let text = pstring::parse_extended_pstring(&data[start..]).unwrap();

            let mut length = text.len() + 2;

            // Count pad bytes
            if length % 2 != 0 {
                length += 1;
            }

            start += length;

            let comment = Comment {
                time_stamp,
                marker,
                text,
            };

            comments.push(comment);
        }

        let chunk = Chunk::CommentsChunk { comments };

        return Ok(chunk);
    }

    fn parse_application_specific_chunk(data: &[u8]) -> io::Result<Chunk> {
        let application_signature = String::from_utf8(data[..4].to_vec()).unwrap();

        // let data_str = String::from_utf8(data[4..].to_vec()).unwrap();
        // println!("{}", data_str);

        let data = Vec::from(&data[4..]);

        let chunk = Chunk::ApplicationSpecificChunk {
            application_signature,
            data,
        };

        return Ok(chunk);
    }

    fn parse_instrument_chunk(data: &[u8]) -> io::Result<Chunk> {
        let chunk_data = util::read_struct::<InstrumentChunkData, &[u8]>(data)?;

        let sustain_loop = Loop::from(chunk_data.sustain_loop)?;
        let release_loop = Loop::from(chunk_data.release_loop)?;

        let chunk = Chunk::InstrumentChunk {
            base_frequency: chunk_data.base_frequency,
            detune: chunk_data.detune,
            low_frequency: chunk_data.low_frequency,
            high_frequency: chunk_data.high_frequency,
            low_velocity: chunk_data.low_velocity,
            high_velocity: chunk_data.high_velocity,
            gain: chunk_data.gain,
            sustain_loop: sustain_loop,
            release_loop: release_loop,
        };

        return Ok(chunk);
    }

    fn parse_id3_chunk(data: &[u8]) -> io::Result<Chunk> {
        let data = Vec::from(data);

        let chunk = Chunk::ID3Chunk { data };

        return Ok(chunk);
    }

    fn parse_name_chunk(data: &[u8]) -> io::Result<Chunk> {
        let text = parse_text(data).expect("Parse UTF-8 string");

        let chunk = Chunk::NameChunk { text };

        return Ok(chunk);
    }

    fn parse_author_chunk(data: &[u8]) -> io::Result<Chunk> {    
        let text = parse_text(data).expect("Parse UTF-8 string");

        let chunk = Chunk::AuthorChunk { text };

        return Ok(chunk);
    }

    fn parse_copyright_chunk(data: &[u8]) -> io::Result<Chunk> {    
        let text = parse_text(data).expect("Parse UTF-8 string");

        let chunk = Chunk::CopyrightChunk { text };

        return Ok(chunk);
    }

    fn parse_annotation_chunk(data: &[u8]) -> io::Result<Chunk> {
        let text = parse_text(data).expect("Parse UTF-8 string");

        let chunk = Chunk::AnnotationChunk { text };

        return Ok(chunk);
    }

    fn get_size(&self) -> usize {
        CHUNK_HEADER_SIZE
            + match self {
                Chunk::CommonChunk {
                    num_channels: _,
                    num_sample_frames: _,
                    sample_size: _,
                    sample_rate: _,
                } => mem::size_of::<CommonChunkData>(),
                Chunk::SoundDataChunk {
                    offset: _,
                    block_size: _,
                    data,
                } => 8 + data.len(),
                Chunk::MarkerChunk { markers } => {
                    let mut size = 2;

                    for marker in markers {
                        size += 6 + marker.name.len();
                    }

                    size
                }
                Chunk::CommentsChunk { comments } => {
                    unimplemented!();
                }
                Chunk::ApplicationSpecificChunk {
                    application_signature: _,
                    data,
                } => 4 + data.len(),
                Chunk::InstrumentChunk {
                    base_frequency: _,
                    detune: _,
                    low_frequency: _,
                    high_frequency: _,
                    low_velocity: _,
                    high_velocity: _,
                    gain: _,
                    sustain_loop: _,
                    release_loop: _,
                } => mem::size_of::<InstrumentChunkData>(),
                Chunk::ID3Chunk { data } => data.len(),
                Chunk::NameChunk { text } => text.len(),
                Chunk::AuthorChunk { text } => text.len(),
                Chunk::CopyrightChunk { text } => text.len(),
                Chunk::AnnotationChunk { text } => text.len(),
                Chunk::UnsupportedChunk {
                    id: _,
                    size: _,
                    data,
                } => data.len(),
            }
    }

    fn marshal(&self) -> Vec<u8> {
        match self {
            Chunk::CommonChunk {
                num_channels,
                num_sample_frames,
                sample_size,
                sample_rate,
            } => {
                // TODO: write header

                let common_chunk_data = CommonChunkData {
                    num_channels: *num_channels as i16,
                    num_sample_frames: num_sample_frames.to_be_bytes(),
                    sample_size: *sample_size as i16,
                    sample_rate: util::f64_to_extended(*sample_rate),
                };

                let data = util::write_struct::<CommonChunkData>(&common_chunk_data);
                Vec::from(data)
            }
            Chunk::SoundDataChunk {
                offset,
                block_size,
                data,
            } => {
                // TODO: write header

                let mut output = Vec::with_capacity(8 + data.len());
                output.write_all(&offset.to_be_bytes()).unwrap();
                output.write_all(&block_size.to_be_bytes()).unwrap();
                output.write_all(data).unwrap();
                output
            }
            Chunk::MarkerChunk { markers } => {
                // TODO: implement
                Vec::new()
            }
            Chunk::CommentsChunk { comments } => {
                // TODO: implement
                Vec::new()
            }
            Chunk::ID3Chunk { data } => {
                let mut output = Vec::with_capacity(data.len());
                output.write_all(data).unwrap();
                output
            }
            Chunk::ApplicationSpecificChunk {
                application_signature,
                data,
            } => {
                // TODO: write header
                let mut output = Vec::with_capacity(8 + data.len());
                output.write_all(application_signature.as_bytes()).unwrap();
                output.write_all(data).unwrap();
                output
            }
            Chunk::InstrumentChunk {
                base_frequency,
                detune,
                low_frequency,
                high_frequency,
                low_velocity,
                high_velocity,
                gain,
                sustain_loop,
                release_loop,
            } => {
                // TODO: implement
                Vec::new()
            }
            Chunk::NameChunk {
                text,
            } => {
                unimplemented!()
            }
            Chunk::AuthorChunk {
                text,
            } => {
                unimplemented!()
            }
            Chunk::CopyrightChunk {
                text,
            } => {
                unimplemented!()
            }
            Chunk::AnnotationChunk {
                text,
            } => {
                unimplemented!()
            }
            Chunk::UnsupportedChunk { id, size, data } => {
                // TODO: write header
                let mut output = Vec::with_capacity(8 + data.len());
                output.write_all(id.as_bytes()).unwrap();
                output.write_all(&size.to_be_bytes()).unwrap();
                output
            }
        }
    }
}

impl Debug for Chunk {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Chunk::CommonChunk {
                num_channels,
                num_sample_frames,
                sample_size,
                sample_rate,
            } => formatter
                .debug_struct("CommonChunk")
                .field("num_channels", num_channels)
                .field("num_sample_frames", num_sample_frames)
                .field("sample_size", sample_size)
                .field("sample_rate", sample_rate)
                .finish(),

            Chunk::SoundDataChunk {
                offset,
                block_size,
                data,
            } => formatter
                .debug_struct("SoundDataChunk")
                .field("offset", offset)
                .field("block_size", block_size)
                .field("size", &data.len())
                .finish(),

            Chunk::MarkerChunk { markers } => formatter
                .debug_struct("MarkerChunk")
                .field("markers", markers)
                .finish(),

            Chunk::ApplicationSpecificChunk {
                application_signature,
                data: _,
            } => formatter
                .debug_struct("ApplicationSpecificChunk")
                .field("application_signature", application_signature)
                .finish(),

            Chunk::InstrumentChunk {
                base_frequency,
                detune,
                low_frequency,
                high_frequency,
                low_velocity,
                high_velocity,
                gain,
                sustain_loop,
                release_loop,
            } => formatter
                .debug_struct("InstrumentChunk")
                .field("base_frequency", base_frequency)
                .field("detune", detune)
                .field("low_frequency", low_frequency)
                .field("high_frequency", high_frequency)
                .field("low_velocity", low_velocity)
                .field("high_velocity", high_velocity)
                .field("gain", gain)
                .field("sustain_loop", sustain_loop)
                .field("release_loop", release_loop)
                .finish(),

            Chunk::CommentsChunk { comments } => formatter
                .debug_struct("CommentsChunk")
                .field("comments", comments)
                .finish(),

            Chunk::ID3Chunk { data } => formatter
                .debug_struct("ID3Chunk")
                .field("size", &data.len())
                .finish(),

            Chunk::NameChunk { text } => formatter
                .debug_struct("NameChunk")
                .field("text", text)
                .finish(),

            Chunk::AuthorChunk { text } => formatter
                .debug_struct("AuthorChunk")
                .field("text", text)
                .finish(),

            Chunk::CopyrightChunk { text } => formatter
                .debug_struct("CopyrightChunk")
                .field("text", text)
                .finish(),

            Chunk::AnnotationChunk { text } => formatter
                .debug_struct("AnnotationChunk")
                .field("text", text)
                .finish(),

            Chunk::UnsupportedChunk { id, size, data: _ } => formatter
                .debug_struct("UnsupportedChunk")
                .field("id", id)
                .field("size", size)
                .finish(),
        }
    }
}

pub struct File {
    source: fs::File,
    chunks: Vec<Chunk>,
    size: usize,
}

impl File {
    pub fn create(path: &path::Path) -> io::Result<File> {
        let source = fs::File::create(path)?;

        let file = File {
            source: source,
            chunks: Vec::new(),
            size: 0,
        };

        return Ok(file);
    }

    pub fn open(path: &path::Path) -> io::Result<File> {
        let mut source = fs::File::open(path)?;
        let mut buffer = Vec::new();

        source.read_to_end(&mut buffer)?;

        log::debug!("File size: {}", buffer.len());

        let size = File::parse_format_header(&buffer)?;

        let mut chunks = Vec::new();

        let mut offset = 12;

        while offset < buffer.len() {
            log::trace!("Next offset: {}", offset);

            if buffer[offset..].len() < CHUNK_HEADER_SIZE {
                break;
            }

            let (chunk, size) = Chunk::from(&buffer[offset..])?;
            chunks.push(chunk);

            offset = offset + CHUNK_HEADER_SIZE + size;
        }

        log::trace!("End offset: {}", offset);

        let file = File {
            source,
            size,
            chunks,
        };

        Ok(file)
    }

    fn parse_format_header(data: &[u8]) -> io::Result<usize> {
        let mut header = util::read_struct::<FormatHeader, &[u8]>(data)?;
        header.size = header.size.to_be();

        if &header.id != b"FORM" {
            return Err(io::Error::new(
                io::ErrorKind::Other,
                "file format is not IFF",
            ));
        }

        if &header.format != b"AIFF" {
            return Err(io::Error::new(
                io::ErrorKind::Other,
                "file format is not AIFF",
            ));
        }

        return Ok(header.size as usize);
    }

    pub fn set_sound_data(&mut self, sound_data: SoundData) {
        let sound_data_chunk = Chunk::SoundDataChunk {
            offset: sound_data.offset,
            block_size: sound_data.block_size,
            data: sound_data.data,
        };

        let common_chunk = Chunk::CommonChunk {
            num_channels: sound_data.num_channels,
            num_sample_frames: sound_data.num_sample_frames,
            sample_size: sound_data.sample_size,
            sample_rate: sound_data.sample_rate,
        };

        self.chunks.push(common_chunk);
        self.chunks.push(sound_data_chunk);

        self.update_size();
    }

    pub fn set_markers(&mut self, markers: Vec<Marker>) {
        // TODO:
        unimplemented!();
    }

    pub fn set_app_data(&mut self, application: String, data: Vec<u8>) {
        // TODO:
        unimplemented!();
    }

    pub fn set_instrument(&mut self, instrument: Instrument) {
        // TODO:
        unimplemented!();
    }

    pub fn set_midi_data(&mut self, data: Vec<u8>) {
        // TODO:
        unimplemented!();
    }

    pub fn set_aes_channel_status_data(&mut self, data: [u8; 24]) {
        // TODO:
        unimplemented!();
    }

    pub fn set_comments(&mut self, comments: Vec<Comment>) {
        // TODO:
        unimplemented!();
    }

    pub fn set_name(&mut self, text: String) {
        // TODO:
        unimplemented!();
    }

    pub fn set_author(&mut self, text: String) {
        // TODO:
        unimplemented!();
    }

    pub fn set_copyright(&mut self, text: String) {
        // TODO:
        unimplemented!();
    }

    pub fn set_annotation(&mut self, text: String) {
        // TODO:
        unimplemented!();
    }

    pub fn save(&mut self) -> io::Result<()> {
        let mut buf_writer = BufWriter::new(&self.source);

        let format_header = FormatHeader {
            id: b"FORM".to_owned(),
            size: self.size as i32,
            format: b"AIFF".to_owned(),
        };

        let form_data = util::write_struct::<FormatHeader>(&format_header);

        buf_writer.write_all(form_data)?;

        for chunk in &self.chunks {
            let data = chunk.marshal();
            buf_writer.write_all(&data)?;
        }

        buf_writer.flush()
    }

    fn update_size(&mut self) {
        self.size = 0;

        for chunk in &self.chunks {
            self.size += chunk.get_size();
        }
    }
}

pub struct SoundData {
    num_channels: u16,
    num_sample_frames: u32,
    sample_size: u16,
    sample_rate: f64,
    data: Vec<u8>,
    offset: u32,
    block_size: u32,
}

impl SoundData {
    pub fn new(
        num_channels: u16,
        num_sample_frames: u32,
        sample_size: u16,
        sample_rate: f64,
        data: Vec<u8>,
    ) -> SoundData {
        // TODO: add validation

        SoundData {
            num_channels,
            num_sample_frames,
            sample_size,
            sample_rate,
            data,
            offset: 0,
            block_size: 0,
        }
    }
}

struct ScopeCall<F: FnOnce()> {
    c: Option<F>,
}

impl<F: FnOnce()> Drop for ScopeCall<F> {
    fn drop(&mut self) {
        self.c.take().unwrap()()
    }
}

macro_rules! expr {
    ($e: expr) => {
        $e
    };
}

macro_rules! defer {
    ($($data: tt)*) => (
        let _scope_call = ScopeCall {
            c: Some(|| -> () { expr!({ $($data)* }) })
        };
    )
}

fn parse_text(data: &[u8]) -> Result<String, string::FromUtf8Error> {
    if data.is_empty() {
        Ok(String::new())
    } else {
        String::from_utf8(data.to_vec())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::env;
    use std::collections::HashMap;
    use path::Path;

    #[test]
    fn test_open_file() {

        let mut test_cases = HashMap::new();
        // test_cases.insert(Path::new("test/Synthwave Arp 01_C.aif"), 10);
        test_cases.insert(Path::new("test/Pain 01-Bass.aif"), 10);
        // test_cases.insert(Path::new("test/shed_builder_120_intro_hat2.aiff"), 9);
        // test_cases.insert(Path::new("test/mosha175_bridge_ride2.aiff"), 9);
        // test_cases.insert(Path::new("test/84BPM_SRK_01_Loop.aiff"), 9);
        // test_cases.insert(Path::new("test/Basso.aiff"), 2);
        // test_cases.insert(Path::new("test/Submarine.aiff"), 2);
        // test_cases.insert(Path::new("test/Tink.aiff"), 2);
        // test_cases.insert(Path::new("test/oberheimdmx-4760.aif"), 5);
        // test_cases.insert(Path::new("test/048-bass1.aif"), 3);

        for (path, count) in test_cases {
            let result = File::open(path);
            assert_eq!(result.is_ok(), true);
            let file = result.unwrap();

            println!("{:?}", file.chunks);
            assert_eq!(count, file.chunks.len());
        }
    }

    #[test]
    fn test_create_file() -> io::Result<()> {
        let mut new_file = env::temp_dir();
        new_file.push("new.aif");

        println!("Creating {:?}", new_file);

        let mut file = File::create(&new_file)?;
        defer! {
            fs::remove_file(new_file).unwrap();
        }

        let num_channels = 2;
        let num_sample_frames = 0;
        let sample_size = 16;
        let sample_rate = 44.1;

        let data = vec![
            0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0x0F,
        ];

        let sound_data = SoundData::new(
            num_channels,
            num_sample_frames,
            sample_size,
            sample_rate,
            data,
        );

        file.set_sound_data(sound_data);

        file.save()?;

        return Ok(());
    }
}
