use std::convert::TryInto;
use std::io::Write;

/// 80 bit MacOS float: 1 sign bit, 15 exponent bits, 1 integer bit, 63 fraction bits.
#[derive(Debug, PartialEq)]
pub struct Float80 {
    exp: u16,
    man: u64,
}

impl Float80 {
    pub fn new(exp: u16, man: u64) -> Float80 {
        Float80 { exp, man }
    }

    pub fn from_bytes(data: [u8; 10]) -> Float80 {
        let exp = u16::from_be_bytes(data[..2].try_into().unwrap());

        let man = u64::from_be_bytes(data[2..].try_into().unwrap());

        Float80 { exp, man }
    }

    pub fn from_f64(x: f64) -> Float80 {
        // Sign, exponent and fraction of binary64.
        //
        //    1 bit:   sign
        //    11 bits: exponent
        //    52 bits: fraction
        let bits = x.to_bits();

        // 1 bit: sign
        let sign = (bits >> 63) as u16;

        // 11 bits: exponent
        let exp = bits >> 52 & 0x7FF;

        // 52 bits: fraction
        let frac = bits & 0xFFFFFFFFFFFFF;

        if exp == 0 && frac == 0 {
            // zero value.
            let se = sign << 15;

            let value = Float80 { exp: se, man: 0 };

            return value;
        }

        // Sign, exponent and fraction of binary80.
        //
        //    1 bit:   sign
        //    15 bits: exponent
        //    1 bit:   integer part
        //    63 bits: fraction

        // 15 bits: exponent.
        //
        // Exponent bias 1023  (binary64)
        // Exponent bias 16383 (binary80)
        let mut exp80 = (exp as i64) - 1023 + 16383;

        // 63 bits: fraction.
        //
        let frac80 = frac << 11;

        if exp == 0 {
            exp80 = 0
        } else if exp == 0x7FF {
            exp80 = 0x7FFF
        }

        let se = sign << 15 | (exp80 as u16);

        // Integer part set to specify normalized value.

        // Handle NaN.
        if x.is_nan() {
            return Float80::new(se, 0xC000000000000000);
        }

        let m = 0x8000000000000000 | frac80;

        return Float80::new(se, m);
    }

    pub fn into_f64(self) -> f64 {
        let se = self.exp as u64;
        let m = self.man;

        // 1 bit: sign
        let sign = se >> 15;

        // 15 bits: exponent
        let exp = se & 0x7FFF;

        // Adjust for exponent bias.
        //
        // === [ binary64 ] =========================================================
        //
        // Exponent bias 1023.
        //
        //    +===========================+=======================+
        //    | Exponent (in binary)      | Notes                 |
        //    +===========================+=======================+
        //    | 00000000000               | zero/subnormal number |
        //    +---------------------------+-----------------------+
        //    | 00000000001 - 11111111110 | normalized value      |
        //    +---------------------------+-----------------------+
        //    | 11111111111               | infinity/NaN          |
        //    +---------------------------+-----------------------+
        //
        // References:
        //    https://en.wikipedia.org/wiki/Double-precision_floating-point_format#Exponent_encoding
        let mut exp64 = exp as i64 - 16383 + 1023;

        if exp == 0 {
            // exponent is all zeroes.
            exp64 = 0
        } else if exp == 0x7FFF {
            if m == 0xC000000000000000 {
                // Handle NaN.
                return f64::NAN;
            }

            // exponent is all ones.
            exp64 = 0x7FF
        }

        // 63 bits: fraction
        let frac = m & 0x7FFFFFFFFFFFFFFF;

        // Sign, exponent and fraction of binary64.
        //
        //    1 bit:   sign
        //    11 bits: exponent
        //    52 bits: fraction
        //
        // References:
        //    https://en.wikipedia.org/wiki/Double-precision_floating-point_format#IEEE_754_double-precision_binary_floating-point_format:_binary64
        let bits = sign << 63 | (exp64 as u64) << 52 | frac >> 11;

        return f64::from_bits(bits);
    }

    pub fn into_bytes(self) -> [u8; 10] {
        let mut bytes = Vec::with_capacity(10);

        let exp_bytes = self.exp.to_be_bytes();
        bytes.write_all(&exp_bytes).unwrap();

        let man_bytes = self.man.to_be_bytes();
        bytes.write_all(&man_bytes).unwrap();

        let mut data = [0_u8; 10];

        // io::copy(&mut reader, &mut data).unwrap();
        for (i, value) in bytes.into_iter().enumerate() {
            data[i] = value;
        }

        return data;
    }

    pub fn to_string(&self) -> String {
        format!("{:04X}{:016X}", self.exp, self.man)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_float80() {
        let expected = Float80 {
            exp: 16398,
            man: 12413046472939929600,
        };

        let data = [0x40, 0x0E, 0xAC, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];

        let value = Float80::from_bytes(data);

        assert_eq!(expected, value);
    }

    #[test]
    fn test_float80_into_bytes() {
        let value = Float80 {
            exp: 16398,
            man: 12413046472939929600,
        };

        let expected = [0x40, 0x0E, 0xAC, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];

        let data = value.into_bytes();

        assert_eq!(expected, data);
    }

    #[test]
    fn test_float80_to_string() {
        let value = Float80 {
            exp: 16398,
            man: 12413046472939929600,
        };

        let expected = "400EAC44000000000000";

        let string = value.to_string();

        assert_eq!(expected, string);
    }

    // #[test]
    // fn test_f64_from_extended() {
    //     let data = [0x40, 0x0E, 0xAC, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];

    //     let value = f64_from_extended(data);

    //     assert_eq!(44100.0, value);
    // }
}
