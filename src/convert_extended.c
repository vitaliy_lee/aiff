#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define INTEL_CPU

#ifdef INTEL_CPU

/* *************************** FlipLong() ******************************
 * Converts a long in "Big Endian" format (ie, Motorola 68000) to Intel
 * reverse-byte format, or vice versa if originally in Big Endian.
 ********************************************************************* */
void FlipLong(unsigned char* ptr) {
	register unsigned char val;

	/* Swap 1st and 4th bytes */
	val = *(ptr);
	*(ptr) = *(ptr+3);
	*(ptr+3) = val;

	/* Swap 2nd and 3rd bytes */
	ptr += 1;
	val = *(ptr);
	*(ptr) = *(ptr+1);
	*(ptr+1) = val;
}

#endif

/* ************************* FetchLong() *******************************
 * Fools the compiler into fetching a long from a char array.
 ********************************************************************* */

unsigned long FetchLong(unsigned long* ptr) {
	return(*ptr);
}

/* ************************* ConvertFloat() *****************************
 * Converts an 80 bit IEEE Standard 754 floating point number to an unsigned
 * long.
 ********************************************************************** */

unsigned long ConvertFloat(unsigned char* buffer) {
	unsigned long mantissa;
	unsigned long last = 0;
	unsigned char exp;

	#ifdef INTEL_CPU
	FlipLong((unsigned long *)(buffer+2));
	#endif

	mantissa = FetchLong((unsigned long*)(buffer+2));
	exp = 30 - *(buffer+1);
	
	while (exp--) {
		last = mantissa;
		mantissa >>= 1;
	}
	 
	if (last & 0x00000001) {
		mantissa++;
	}

	return mantissa;
}

/* ************************* StoreLong() ******************************
 * Fools the compiler into storing a long into a char array.
 ******************************************************************** */

void StoreLong(unsigned long val, unsigned long* ptr) {
	*ptr = val;
}

/* ************************** StoreFloat() ******************************
 * Converts an unsigned long to 80 bit IEEE Standard 754 floating point
 * number.
 ********************************************************************** */

void StoreFloat(unsigned char* buffer, unsigned long value) {
	unsigned long exp;
	unsigned char i;

	memset(buffer, 0, 10);

	exp = value;
	exp >>= 1;
	 
	for (i=0; i<32; i++) { exp>>= 1;
		if (!exp) {
			break;
		}
	}

	*(buffer+1) = i;

	for (i=32; i; i--) {
		if (value & 0x80000000) {
			break;
		}

		value <<= 1;
	}

	StoreLong(value, buffer+2);

	#ifdef INTEL_CPU
	FlipLong((unsigned long *)(buffer+2));
	#endif
}

unsigned char* bin_to_strhex(const unsigned char *bin, unsigned int binsz, unsigned char **result) {
	unsigned char hex_str[]= "0123456789abcdef";
	unsigned int i;

	if (!(*result = (unsigned char *)malloc(binsz * 2 + 1))) {
    	return (NULL);
	}

  	(*result)[binsz * 2] = 0;

	if (!binsz) {
    	return (NULL);
    }

 	for (i = 0; i < binsz; i++) {
		(*result)[i * 2 + 0] = hex_str[(bin[i] >> 4) & 0x0F];
		(*result)[i * 2 + 1] = hex_str[(bin[i]     ) & 0x0F];
	}

	return (*result);
}

int main() {
	// Read
	unsigned char buffer[] = {0x40, 0x0E, 0xAC, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

	unsigned char* hexstr;
	
	bin_to_strhex(&buffer, 10, &hexstr);

	printf("input value: %s\n", hexstr);

	unsigned long value = ConvertFloat(buffer);

	printf("converted value: %lu\n", value);


	// Store
	value = 192000;

	printf("input value: %lu\n", value);

	StoreFloat(&buffer, value);

	bin_to_strhex(&buffer, 10, &hexstr);

	printf("stored value: %s\n", hexstr);
}
