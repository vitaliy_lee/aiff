use std::io::{self, Read};
use std::mem;
use std::slice;

use crate::float80::Float80;

pub fn read_struct<T, R: Read>(mut read: R) -> io::Result<T> {
    let size = mem::size_of::<T>();

    unsafe {
        let mut data = mem::zeroed();

        let buffer = slice::from_raw_parts_mut(&mut data as *mut T as *mut u8, size);

        match read.read_exact(buffer) {
            Ok(()) => Ok(data),
            Err(error) => {
                mem::forget(data);
                Err(error)
            }
        }
    }
}

pub fn write_struct<T: Sized>(p: &T) -> &[u8] {
    let size = mem::size_of::<T>();

    unsafe { slice::from_raw_parts((p as *const T) as *const u8, size) }
}

pub fn f64_from_extended(data: [u8; 10]) -> f64 {
    Float80::from_bytes(data).into_f64()
}

pub fn f64_to_extended(value: f64) -> [u8; 10] {
    Float80::from_f64(value).into_bytes()
}
