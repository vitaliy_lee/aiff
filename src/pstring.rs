use std::convert::TryInto;
use std::error::Error;
use std::fmt;

type Result<T> = std::result::Result<T, Box<dyn Error>>;

// Define our error types. These may be customized for our error handling cases.
// Now we will be able to write our own errors, defer to an underlying error
// implementation, or do something in between.
#[derive(Debug, Clone)]
struct ParseError(String);

impl ParseError {
    fn new(message: &str) -> Box<ParseError> {
        Box::new(ParseError(String::from(message)))
    }
}

impl Error for ParseError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        // Generic error, underlying cause isn't tracked.
        None
    }
}

// Generation of an error is completely separate from how it is displayed.
// There's no need to be concerned about cluttering complex logic with the display style.
//
// Note that we don't store any extra info about the errors. This means we can't state
// which string failed to parse without modifying our types to carry that information.
impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "invalid first item to double")
    }
}

pub fn parse_pstring(data: &[u8]) -> Result<String> {
    if data.is_empty() {
        // return Err("data is empty");
        return Ok(String::new());
    }

    let length = data[0] as usize;

    let start = 1;
    let mut end = data.len();

    // Skip pad byte
    if length % 2 == 0 {
        end -= 1;
    }

    let string_data = &data[start..end];

    if string_data.len() != length {
        return Err(ParseError::new("length mismatch"));
    }

    if length == 0 {
        return Ok(String::new());
    }

    match String::from_utf8(string_data.to_vec()) {
        Ok(value) => Ok(value),
        Err(error) => Err(ParseError::new(&error.to_string())),
    }
}

pub fn parse_extended_pstring(data: &[u8]) -> Result<String> {
    if data.is_empty() {
        return Ok(String::new());
    }

    let buffer: [u8; 2] = data[..2].try_into().unwrap();
    let length = u16::from_be_bytes(buffer) as usize;

    if length == 0 {
        return Ok(String::new());
    }

    let start = 2;
    let end = start + length;

    let string_data = &data[start..end];

    match String::from_utf8(string_data.to_vec()) {
        Ok(value) => Ok(value),
        Err(error) => Err(ParseError::new(&error.to_string())),
    }
}

pub fn to_pstring_bytes(value: &str) -> Vec<u8> {
    let mut data = value.to_string().into_bytes();
    data.insert(0, data.len() as u8);

    // Add pad byte
    if data.len() % 2 != 0 {
        data.push(0);
    }

    return data;
}

pub fn size(value: &str) -> usize {
    let mut size = value.len() + 1;

    if size % 2 != 0 {
        size += 1;
    }

    return size;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_pstring() {
        let mut message = "Apple";
        let mut data = message.to_string().into_bytes();
        data.insert(0, message.len() as u8);

        let mut value = parse_pstring(&data).unwrap();
        assert_eq!(message, value.as_str());

        message = "Lisa";
        data = message.to_string().into_bytes();
        data.insert(0, message.len() as u8);
        data.push(0);

        value = parse_pstring(&data).unwrap();
        assert_eq!(message, value.as_str());
    }

    #[test]
    fn test_to_pstring_bytes() {
        let mut message = "Apple";

        let mut expected = message.to_string().into_bytes();
        expected.insert(0, message.len() as u8);

        let mut value = to_pstring_bytes(&message);
        assert_eq!(expected, value);
        assert!(value.len() % 2 == 0);

        message = "Lisa";
        expected = message.to_string().into_bytes();
        expected.insert(0, message.len() as u8);
        expected.push(0);

        value = to_pstring_bytes(&message);
        assert_eq!(expected, value);
        assert!(value.len() % 2 == 0);
    }
}
