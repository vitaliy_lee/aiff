# Common Chunk 

One, and only one, is required in every FORM AIFF.

# Sound Data Chunk

is required unless the numSampleFrames field in the Common Chunk is zero.
One, and only one, Sound Data Chunk may appear in a FORM AIFF.

# Marker Chunk

is optional. No more than one Marker Chunk can appear in a FORM AIFF.

# Instrument Chunk

is optional. No more than 1 Instrument Chunk can appear in one FORM AIFF.

# MIDI Data Chunk

is optional. Any number of these chunks may exist in one FORM AIFF. If MIDI System Exclusive messages for several instruments are to be stored in a FORM AIFF, it is better to use one MIDI Data Chunk per instrument than one big MIDI Data Chunk for all of the instruments.

# Audio Recording Chunk

is optional. No more than 1 Audio Recording Chunk may appear in one FORM AIFF.

# Application Specific Chunk

is optional. Any number of these chunks may exist in a one FORM AIFF.

# Comments Chunk

is optional. No more than 1 Comments Chunk may appear in one FORM AIFF.

# Text Chunks, Name, Author, Copyright, Annotation

optional chunks
